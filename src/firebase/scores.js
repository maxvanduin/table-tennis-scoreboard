import { db } from '../firebase';
import { calcNewPointsAmount } from '../helpers/pointsCalculation';

async function saveScores(winner, loser, winningPercentage, user) {
  const batch = db.batch();
  const winningRef = db.collection('players').doc(winner.uid);
  const losingRef = db.collection('players').doc(loser.uid);
  const newWinnerScore = calcNewPointsAmount(winner, loser, winningPercentage);
  const newLoserScore = calcNewPointsAmount(loser, winner, 1 - winningPercentage);
  batch.update(winningRef, { score: newWinnerScore });
  batch.update(losingRef, { score: newLoserScore });
  await batch.commit();
  
  winner.uid === user.uid
    ? user.score = calcNewPointsAmount(winner, loser, winningPercentage)
    : user.score = calcNewPointsAmount(loser, winner, 1 - winningPercentage);
}

async function reset(user, previousScores) {
  const batch = db.batch();
  const userRef = db.collection('players').doc(previousScores.user.uid);
  const rivalRef = db.collection('players').doc(previousScores.rival.uid);
  batch.update(userRef, { score: previousScores.user.score });
  batch.update(rivalRef, { score: previousScores.rival.score });
  await batch.commit();
  user.score = previousScores.user.score;
}

export {
  saveScores,
  reset,
}