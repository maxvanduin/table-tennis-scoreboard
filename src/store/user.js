import { writable } from 'svelte/store';

export const user = writable({
  displayName: '',
  score: null,
  photoURL: '',
  uid: '',
})

export const userExists = writable(false);