const kFactor = 32;

function calcRating(user) {
  return Math.pow(10, (user.score / 400));
}

function calcChanceOfWinning(user, challenger) {
  if (user && challenger) {
    return calcRating(user) / (calcRating(challenger) + calcRating(user));
  }
}

function calcNewPointsAmount(player, competitor, winOrLose) {
  const chanceOfWinning = calcChanceOfWinning(player, competitor);
  const newTotalPlayerScore = player.score + kFactor * (winOrLose - chanceOfWinning);
  return Math.round(newTotalPlayerScore);
}

export {
  calcChanceOfWinning,
  calcNewPointsAmount,
}