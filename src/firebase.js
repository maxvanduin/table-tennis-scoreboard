import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
const firebaseConfig = {
    apiKey: "AIzaSyCiOKyQ7e0s_jV-FbEjryJySFfY3u5iBhc",
    authDomain: "table-tennis-scoreboard-edf48.firebaseapp.com",
    databaseURL: "https://table-tennis-scoreboard-edf48.firebaseio.com",
    projectId: "table-tennis-scoreboard-edf48",
    storageBucket: "",
    messagingSenderId: "859505485897",
    appId: "1:859505485897:web:e83860bd478a9378"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

export const auth = firebase.auth();
export const googleProvider = new firebase.auth.GoogleAuthProvider();

export const db = firebase.firestore();